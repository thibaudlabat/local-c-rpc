//
// Created by thibaud on 31/01/2022.
//

#ifndef LOCAL_C_RPC_RPC_CLIENT_H
#define LOCAL_C_RPC_RPC_CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "consts.h"
#include "utils.h"

typedef struct {
    key_t ipc_key_server; // channel du serveur central
    key_t ipc_key_results; // channel de communication actuel
    int last_call_status_code;// code d'erreur du dernier appel remote
    const char *last_call_error; // message d'erreur du dernier appel remote
    /*
     * codes d'erreur négatifs = erreur du protocole
     * codes d'erreur positifs = erreur de la fonction appelée
     */
    int max_size; // taille maximale d'un paquet de données
} rpc_client_context;

void rpc_init_context(rpc_client_context *context) {
    context->ipc_key_server = IPC_KEY_SERVER_COMMAND;
    context->max_size = 1024; // arbitraire
}

static int CLIENT_VERBOSE = 1;

void *rpc_remote_call(rpc_client_context *context,
                      const char *function,
                      void *input,
                      int input_size,
                      int *output_size) {
    /*
     * Renvoie un ptr vers l'output; à vous de le free
     */
    int msqid_command, msqid_results;
    if ((msqid_command = msgget(context->ipc_key_server, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    int ipc_key_results = 100000 + random() % 20000; // arbitraire

    if ((msqid_results = msgget(ipc_key_results, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    struct my_msgbuf buf;
    buf.mtype = 1; /* we don't really care in this case */ // TODO : ??

    *buf.mtext = '\0';
    sprintf(buf.mtext, "%d %s", ipc_key_results, function);
    strcat(buf.mtext, " ");
    rpc_serialize_unsafe(input, input_size, (char *) buf.mtext);
    int len = strlen(buf.mtext);

    if (msgsnd(msqid_command, &buf, len + 1, 0) == -1) {
        perror("msgsnd");
        exit(1);
    }
    if (CLIENT_VERBOSE) printf(">cmd\twrite:\t%s\n", buf.mtext);

    if (msgrcv(msqid_results, &buf, sizeof(buf.mtext), 0, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }
    if (CLIENT_VERBOSE) printf(">res\tread:\t%s\n", buf.mtext);

    // TODO : delete channel
    /*if (msgctl(msqid_results, IPC_RMID, NULL) == -1) {
        perror("msgctl");
        exit(1);
    }*/


    //printf("result : %s\n", buf.mtext);
    void *output = rpc_unserialize(buf.mtext, output_size);
    if (output == 0) { // gestion d'erreur
        context->last_call_status_code = -1;
        return 0;
    }


    context->last_call_status_code = 0;
    return output;
}

#endif //LOCAL_C_RPC_RPC_CLIENT_H
