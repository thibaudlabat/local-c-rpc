#include <stdio.h>
#include "rpc_client.h"


int main(void) {

    rpc_client_context context;
    rpc_init_context(&context);

    int a = 12;
    int output_size;
    int *r = rpc_remote_call(&context,"square", &a, sizeof(int), &output_size);
    printf("result=%d\n", *r);
    free(r);


    float input[9] = {-1.9f, 2.8f, 3.7f, -4.6f, 5.5f, 6.4f, -7.3f, -8.2f, 9.1f};
    int result_size;
    float *result = rpc_remote_call(&context,"filter_positive_float", input, sizeof(input), &result_size);
    for (int i = 0; i < result_size / sizeof(float); ++i)
        printf("result : %f\n", result[i]);
    free(result);

    void * input2 = 0;
    int r2;
    printf("count to 10\n");
    free(rpc_remote_call(&context,"count_to_10", input2, 0, &r2));
    printf("finished\n");
    return 0;
}
