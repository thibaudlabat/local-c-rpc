//
// Created by thibaud on 31/01/2022.
//

#ifndef LOCAL_C_RPC_RPC_SERVER_H
#define LOCAL_C_RPC_RPC_SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>
#include "consts.h"
#include "utils.h"


// TODO : mettre ça ailleurs ? un context comme pour le client ...
struct rpc_server_context {
    struct {
        const char *name;

        void *(*ptr)(void *, int, int *); // int f(int);
    } functions[100];

    int N_func;
};

struct rpc_server_request_thread_data {
    struct my_msgbuf buf;
    key_t ipc_key_results;
    int id;
    struct rpc_server_context *context;
};


static int SERVER_VERBOSE = 1;


unsigned char *
_rpc_call_func(struct rpc_server_context *context, char *func_name, void *input, int input_size, int *output_size) {
    /* don't forget to free the memory */
    for (int i = 0; i < context->N_func; ++i) {
        if (strcmp(func_name, context->functions[i].name) == 0) {
            return context->functions[i].ptr(input, input_size, output_size);
        }
    }
    printf("function not found : %s\n", func_name);

}


int rpc_server_register(struct rpc_server_context *context, const char *name, void *(*function)(void *, int, int *)) {
    context->functions[context->N_func].name = name;
    context->functions[context->N_func].ptr = function;
    context->N_func++;
}


void _rpc_clear_message_queue(int msqid) {
    struct my_msgbuf buf;
    int r = 0;
    while (msgrcv(msqid, &buf, sizeof(buf.mtext), 0, IPC_NOWAIT) != -1)
        printf("clearing queue %d\n", msqid);

}


// should be called with pthread_create
void *rpc_server_request_thread(void *argPtr) {

    //alias
    struct rpc_server_request_thread_data *threadData = argPtr;
    struct my_msgbuf *buf = &threadData->buf;

    // traitement de la commande en entrée

    printf("received command : %s\n", (*buf).mtext);
    char func_name[64];
    char ipc_key_results_str[64];

    //char delim[] = " ";
    //char *ptr = strtok((*buf).mtext, delim);
    sscanf(buf->mtext, "%s %s", ipc_key_results_str, func_name);
    sscanf(ipc_key_results_str, "%d", &threadData->ipc_key_results);
    int input_size;
    unsigned char *input = rpc_unserialize((*buf).mtext + strlen(func_name) + strlen(ipc_key_results_str) + 1,
                                           &input_size);


    int msqid_results;
    if ((msqid_results = msgget(threadData->ipc_key_results, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    printf("CREATING THREAD n°%d\twith msgqid=%d\n", threadData->id, threadData->ipc_key_results);

    // TODO : gestion d'erreur ici
// si erreur lors de la déserialisation, envoyer au client l'erreur !

    int output_size;
    unsigned char *output = _rpc_call_func(threadData->context, func_name, input, input_size, &output_size);
    free(input);
    printf("sizes : input %d output %d\n", input_size, output_size); // debug

    (*buf).mtype = 1; /* we don't really care in this case */
    (*buf).mtext[0] = 0;
    rpc_serialize_unsafe(output, output_size, (*buf).mtext);
    free(output);
    int len = strlen((*buf).mtext);

    if (msgsnd(msqid_results, buf, len + 1, 0) == -1) {
        perror("msgsnd");
        exit(1);
    }

    free(threadData);
    return NULL;
}


void rpc_server_loop_step(struct rpc_server_context *context, int msqid_command) {
    static int threadCount = 0;

    struct rpc_server_request_thread_data *threadData = malloc(sizeof(struct rpc_server_request_thread_data));
    threadData->context = context;
    if (threadData == 0) {
        printf("no memory left");
        exit(-1);
    }


    if (msgrcv(msqid_command, &(threadData->buf), sizeof((threadData->buf).mtext), 0, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }
    if (SERVER_VERBOSE) printf("[MAIN]\tread:\t%s\n", threadData->buf.mtext);

    threadData->id = threadCount++;
    //rpc_server_request_thread(threadData);
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, rpc_server_request_thread, threadData);


}


int rpc_server_loop(struct rpc_server_context *context) {

    int msqid_command;
    if ((msqid_command = msgget(IPC_KEY_SERVER_COMMAND, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    _rpc_clear_message_queue(msqid_command);

    while (1) {
        rpc_server_loop_step(context, msqid_command);
    }

    pthread_exit(0);
    if (msgctl(msqid_command, IPC_RMID, NULL) == -1) {
        perror("msgctl");
        exit(1);
    }

    printf("message queue: done sending messages.\n");
    return 0;
}


void rpc_server_context_init(struct rpc_server_context *context) {
    memset(context, 0, sizeof(struct rpc_server_context));
//context->N_func=0;
}


#endif //LOCAL_C_RPC_RPC_SERVER_H
