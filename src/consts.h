//
// Created by Thibaud on 29/01/2022.
//

#ifndef LOCAL_C_RPC_CONSTS_H
#define LOCAL_C_RPC_CONSTS_H

#include <sys/types.h>

const key_t IPC_KEY_SERVER_COMMAND = 130000;
// const key_t IPC_KEY_SERVER_RESULTS = 130001; // unused

#define PERMS 0644
struct my_msgbuf {
    long mtype;
    char mtext[10000];
};


#endif //LOCAL_C_RPC_CONSTS_H
