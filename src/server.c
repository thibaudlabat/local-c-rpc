#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rpc_server.h"


int square(int a) {
    return a * a;
}

void *square_wrapper(void *input, int input_size, int *output_size) {
    // assert input_size == sizeof(int)
    *output_size = sizeof(int);
    int *a = malloc(sizeof(int));
    *a = square(*(int *) input);
    return a;
}

void *count_to_10(void *input, int input_size, int *output_size) {
    // assert input_size == sizeof(int)
    *output_size = 0;
    for (int i = 1; i < 11; ++i) {
        printf("%d\n", i);
        sleep(1);
    }
    return malloc(0);
}


void *filter_positive_float(void *input, int input_size, int *output_size) {

    float *input_f = input;
    int t = input_size / sizeof(float);
    float result[10000];
    int n = 0;
    for (int i = 0; i < t; ++i) {
        if (input_f[i] >= 0)
            result[n++] = input_f[i];
    }
    *output_size = n * sizeof(float);
    void *output = malloc(*output_size);
    memcpy(output, result, *output_size);
    return output;
}


int main(int argc, char **argv) {
    struct rpc_server_context context;
    rpc_server_context_init(&context);

    rpc_server_register(&context, "square", &square_wrapper);
    rpc_server_register(&context, "filter_positive_float", &filter_positive_float);
    rpc_server_register(&context, "count_to_10", &count_to_10);

    return rpc_server_loop(&context);
}


