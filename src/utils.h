//
// Created by thibaud on 31/01/2022.
//

#ifndef LOCAL_C_RPC_UTILS_H
#define LOCAL_C_RPC_UTILS_H

#include <stdio.h>
#include <string.h>

void rpc_serialize_unsafe(void *input, int input_size, char *output) {
    char buffer[32];
    sprintf(buffer, "%d", input_size);
    strcat(output, buffer);
    for (int i = 0; i < input_size; ++i) {
        unsigned char *a = input + i;
        unsigned int b = *a;
        strcat(output, " ");
        sprintf(buffer, "%d", b);
        strcat(output, buffer);
    }
}

void rpc_unserialize_unsafe(char *input, void *output, int *output_size) {
    char delim[] = " ";
    char *ptr = strtok(input, delim);
    sscanf(ptr, "%d", output_size);
    unsigned char *output_uc = output;
    for (int i = 0; i < *output_size; ++i) {
        ptr = strtok(NULL, delim);
        int x;
        sscanf(ptr, "%d", &x);
        output_uc[i] = x;
    }
}


void *rpc_unserialize(char *input, int *output_size) { // -> output*
    /*
     * Alloue la mémoire pour l'output -> à l'utilisateur de free.
     */
    char delim[] = " ";
    char *ptr = strtok(input, delim);
    int r = sscanf(ptr, "%d", output_size);
    if (r != 1) return 0;
    unsigned char *output = malloc(*output_size);
    if (output == 0) return 0;
    for (int i = 0; i < *output_size; ++i) {
        ptr = strtok(NULL, delim);
        int x;
        r = sscanf(ptr, "%d", &x);
        if (r != 1) {
            free(output);
            return 0;
        }
        output[i] = x;
    }
    return output;
}

#endif //LOCAL_C_RPC_UTILS_H
