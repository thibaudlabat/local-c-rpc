


#include <time.h>
#include <stdio.h>

#include "rpc_client.h"
#include "rpc_server.h"

int square(int a) {
    return a * a;
}

void *square_wrapper(void *input, int input_size, int *output_size) {
    // assert input_size == sizeof(int)
    *output_size = sizeof(int);
    int *a = malloc(sizeof(int));
    *a = square(*(int *) input);
    return a;
}


void *server_thread(void *arg) {
    rpc_client_context context;
    rpc_init_context(&context);
    rpc_server_register(&context, "square", &square_wrapper);

    rpc_server_loop(&context);
}


int main() {

    printf("Creating server thread \n");
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, server_thread, NULL);

    sleep(1);
    printf("Starting ...\n");
    printf("CLOCKS_PER_SEC = %d\n", CLOCKS_PER_SEC);

    int N = 1000;
    printf("N calls = %d\n", N);


    // normal call
    int _a = 12;
    int _output_size;
    clock_t start = clock();
    int *_r;
    for (int i = 0; i < N; ++i)
        _r = square_wrapper(&_a, sizeof(int), &_output_size);
    clock_t end = clock();
    long times = (end - start);
    printf("Without overhead\t: %ld\n", times);

    // remote call
    rpc_client_context context;
    rpc_init_context(&context);
    int a = 12;
    int output_size;
    start = clock();
    int *r;
    for (int i = 0; i < N; ++i)
        r = rpc_remote_call(&context, "square", &a, sizeof(int), &output_size);

    end = clock();
    long times2 = (end - start);
    printf("With overhead\t: %ld\n", times2);
    printf("diff = %ld\n", times2 - times);
    printf("per call = %f us\n", (double) (times2 - times) / CLOCKS_PER_SEC * 1000000/(double)(N));
}
