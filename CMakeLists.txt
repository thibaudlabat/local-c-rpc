cmake_minimum_required(VERSION 3.16)
project(local_c_rpc C)

set(CMAKE_C_STANDARD 11)

add_executable(client src/client.c src/consts.h src/rpc_server.h src/rpc_client.h src/utils.h)
add_executable(server src/server.c src/consts.h src/rpc_server.h src/rpc_client.h src/utils.h)
add_executable(tests src/tests.c src/consts.h src/rpc_server.h src/rpc_client.h src/utils.h)
