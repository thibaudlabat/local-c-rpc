#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define INPUT_FIFO "INPUT_FIFO"
#define OUTPUT_FIFO "OUTPUT_FIFO"


int doubler(int a) {
    return a * 2;
}

int square(int a) {
    return a * a;
}

struct {
    char *name;

    int (*ptr)(int); // int f(int);
} functions[100];

int N_func = 0;

int call_func(char *func_name, int arg) {
    for (int i = 0; i < N_func; ++i) {
        if (strcmp(func_name, functions[i].name) == 0) {
            return functions[i].ptr(arg);
        }
    }
    return -1;
}

int register_function(const char *name, int(*function)(int)) {
    functions[N_func].name = name;
    functions[N_func].ptr = function;
    N_func++;
}

int rpc_server_loop() {

    char readbuf[1024];
    int must_exit = 0;

    mknod(INPUT_FIFO, S_IFIFO | 0640, 0);
    int input_fd = open(INPUT_FIFO, O_RDONLY | O_NONBLOCK);
    if (input_fd < 0) {
        printf("can't open input FIFO\n");
        return -1;
    }

    mknod(OUTPUT_FIFO, S_IFIFO | 0640, 0);
    int output_fd = open(OUTPUT_FIFO, O_WRONLY);// | O_NONBLOCK);
    if (output_fd < 0) {
        printf("can't open output FIFO\n");
        return -2;
    }

    printf("ready\n");

    while (1) {
        ssize_t read_bytes = read(input_fd, readbuf, sizeof(readbuf));
        readbuf[read_bytes] = '\0';
        if (read_bytes > 0) {
            char func_name[512];
            int func_arg_int;
            //printf("Received string: \"%s\" and length is %d\n", readbuf, (int) strlen(readbuf));
            sscanf(readbuf, "%s %d", func_name, &func_arg_int);
            int return_value = call_func(func_name, func_arg_int);
            printf("call %s\n", func_name);
            printf("return value %d\n", return_value);
            dprintf(output_fd, "%s %d\n", func_name, return_value);
        }

        if (must_exit) {
            close(input_fd);
            close(output_fd);
            break;
        }
    }
}

int main(int argc, char **argv) {
    printf("FIFO RPC Server\n");

    register_function("doubler", &doubler);
    register_function("square", &square);

    return rpc_server_loop();
}
