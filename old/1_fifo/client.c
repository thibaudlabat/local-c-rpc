#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


#define INPUT_FIFO "INPUT_FIFO"
#define OUTPUT_FIFO "OUTPUT_FIFO"

int remote_call(const char *function, int arg) {
    // input/output --> du point de vue du serveur

    mknod(OUTPUT_FIFO, S_IFIFO | 0640, 0);
    int output_fd = open(OUTPUT_FIFO, O_RDONLY | O_NONBLOCK);
    if (output_fd < 0) {
        printf("can't open output FIFO\n");
        return -2;
    }

    mknod(INPUT_FIFO, S_IFIFO | 0640, 0);
    int input_fd = open(INPUT_FIFO, O_WRONLY);// | O_NONBLOCK);
    if (input_fd < 0) {
        printf("can't open input FIFO\n");
        return -1;
    }

    dprintf(input_fd,"%s %d",function,arg);
    usleep(50000); // TODO : synchronisation avec signal

    char f[512];
    int r;
    char readbuf[1024];
    ssize_t read_bytes = read(output_fd, readbuf, sizeof(readbuf));
    readbuf[read_bytes] = '\0';
    sscanf(readbuf,"%s %d",f,&r);
    return r;
}

int main() {
    printf("FIFO RPC Client\n");

    int r1 = remote_call("square",12);
    printf("%d\n",r1);

    int r2 = remote_call("doubler",100);
    printf("%d\n",r2);

    return 0;
}
