//
// Created by thibaud on 31/01/2022.
//

#ifndef LOCAL_C_RPC_RPC_SERVER_H
#define LOCAL_C_RPC_RPC_SERVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "consts.h"
#include "utils.h"

struct {
    const char *name;

    void * (*ptr)(void *, int, int *); // int f(int);
} functions[100];

int N_func = 0;

unsigned char * _rpc_call_func(char *func_name, void *input, int input_size, int *output_size) {
    for (int i = 0; i < N_func; ++i) {
        if (strcmp(func_name, functions[i].name) == 0) {
            return functions[i].ptr(input, input_size, output_size);
          }
    }
    printf("function not found : %s\n", func_name);

}


int rpc_server_register(const char *name, void* (*function)(void *, int, int *)) {
    functions[N_func].name = name;
    functions[N_func].ptr = function;
    N_func++;
}


void _rpc_clear_message_queue(int msqid) {
    struct my_msgbuf buf;
    int r = 0;
    while (msgrcv(msqid, &buf, sizeof(buf.mtext), 0, IPC_NOWAIT) != -1)
        printf("clearing queue %d\n", msqid);

}

int rpc_server_loop() {

    int msqid_command, msqid_results;
    if ((msqid_command = msgget(IPC_KEY_SERVER_COMMAND, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    if ((msqid_results = msgget(IPC_KEY_SERVER_RESULTS, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    _rpc_clear_message_queue(msqid_command);
    _rpc_clear_message_queue(msqid_results);

    struct my_msgbuf buf;

    while (1) {
        if (msgrcv(msqid_command, &buf, sizeof(buf.mtext), 0, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }
        printf("received command : %s\n", buf.mtext);
        char func_name[64];


        char delim[] = " ";
        char *ptr = strtok(buf.mtext, delim);
        sscanf(ptr, "%s", func_name);

        int input_size;
        unsigned char *input = rpc_unserialize(buf.mtext + strlen(func_name) + 1, &input_size);

        int output_size;
        unsigned char * output = _rpc_call_func(func_name, input, input_size, &output_size);
        free(input);
        printf("sizes : input %d output %d\n", input_size, output_size); // debug

        buf.mtype = 1; /* we don't really care in this case */
        buf.mtext[0] = 0;
        rpc_serialize_unsafe(output, output_size, buf.mtext);
        free(output);
        int len = strlen(buf.mtext);

        if (msgsnd(msqid_results, &buf, len + 1, 0) == -1) {
            perror("msgsnd");
            exit(1);
        }



        /*if (msgctl(msqid, IPC_RMID, NULL) == -1) {
            perror("msgctl");
            exit(1);
        }*/

    }
    printf("message queue: done sending messages.\n");
    return 0;
}


#endif //LOCAL_C_RPC_RPC_SERVER_H
