//
// Created by thibaud on 31/01/2022.
//

#ifndef LOCAL_C_RPC_RPC_CLIENT_H
#define LOCAL_C_RPC_RPC_CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "consts.h"
#include "utils.h"

void *rpc_remote_call(const char *function, void *input, int input_size, int *output_size) {
    /*
     * Renvoie un ptr vers l'output; à vous de le free
     */
    int msqid_command, msqid_results;
    if ((msqid_command = msgget(IPC_KEY_SERVER_COMMAND, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    if ((msqid_results = msgget(IPC_KEY_SERVER_RESULTS, PERMS | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    struct my_msgbuf buf;
    buf.mtype = 1; /* we don't really care in this case */
    strcpy(buf.mtext, function);
    strcat(buf.mtext, " ");
    rpc_serialize_unsafe(input, input_size, (char *) buf.mtext);
    int len = strlen(buf.mtext);


    if (msgsnd(msqid_command, &buf, len + 1, 0) == -1) {
        perror("msgsnd");
        exit(1);
    }

    if (msgrcv(msqid_results, &buf, sizeof(buf.mtext), 0, 0) == -1) {
        perror("msgrcv");
        exit(1);
    }

    //printf("result : %s\n", buf.mtext);
    void *output = rpc_unserialize(buf.mtext, output_size);
    return output;


}

#endif //LOCAL_C_RPC_RPC_CLIENT_H
