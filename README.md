school project ; do not use this


# FIFO RPC
client
```c
printf("%d\n", rpc_remote_call("square",12) );
printf("%d\n", rpc_remote_call("double",100) );
```

server
```c
int square(int a) { return a * a; }
int double_(int a) { return 2 * a; }
...
rpc_server_register("square", &square);
rpc_server_register("double", &double_);
return rpc_server_loop();
```

![FIFO RPC](old/1_fifo/animation.gif)


# Notes

```text
https://www.tutorialspoint.com/inter_process_communication/index.htm

Objectifs
[x] FIFO RPC 1 client 1 server
[ ] Message Queue : 1 client 1 server
[ ] Message Queue : p2p , many clients, many servers, avec coordinateur centralisé (like torrent trackers)
```
